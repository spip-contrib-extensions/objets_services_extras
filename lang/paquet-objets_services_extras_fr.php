<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/objets_services_extras.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'objets_services_extras_description' => 'Gérer des services extras pour les objets',
	'objets_services_extras_nom' => 'Services extras pour objets',
	'objets_services_extras_slogan' => 'Des services extras pour vos objets'
);
